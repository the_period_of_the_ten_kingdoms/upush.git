package com.upush;


import android.content.Context;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class BadgeModule extends ReactContextBaseJavaModule {

    private Context context;

    public BadgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
    }

    @Override
    public String getName() {
        return "BadgeModule";
    }

    @ReactMethod
    public void setCount(int count) {
        BadgeUtils.setCount(count, this.context);
    }

}
