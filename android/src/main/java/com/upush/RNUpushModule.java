/*
 * @Author: shiguo
 * @Date: 2022-04-25 17:56:29
 * @LastEditors: shiguo
 * @LastEditTime: 2022-11-21 10:55:12
 * @FilePath: /@aks-dev/upush/android/src/main/java/com/upush/RNUpushModule.java
 */

package com.upush;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RNUpushModule extends ReactContextBaseJavaModule {

    private static final String TAG = "RNUpushModule";
    private final ReactApplicationContext context;
    private static boolean isInit = false;

    public RNUpushModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
        UMPushHelper.getInstance().reactContext = reactContext;

    }


    @Override
    public String getName() {
        return "RNUpushModule";
    }


    @ReactMethod
    public void init(final Promise promise) {

        String deviceToken = UMPushHelper.getInstance().deviceToken;
        if (deviceToken != null && deviceToken.length() > 0) {
            promise.resolve(deviceToken);
        }else{
            UMCommonHelper.getInstance().init();
            UMPushHelper.getInstance().register(new UMPushHelper.RegisterCallback() {
                @Override
                public void onSuccess(String deviceToken) {
                    promise.resolve(deviceToken);
                }

                @Override
                public void onFailure(String errCode, String errDesc) {
                    promise.resolve(errDesc);
                }
            });
        }
    }


}