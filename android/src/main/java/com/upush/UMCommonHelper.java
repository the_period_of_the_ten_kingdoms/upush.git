/*
 * @Author: shiguo
 * @Date: 2022-05-26 16:33:10
 * @LastEditors: shiguo
 * @LastEditTime: 2022-07-22 17:20:43
 * @FilePath: /@aks-dev/upush/android/src/main/java/com/upush/UMCommonHelper.java
 */
package com.upush;

import android.content.Context;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.PushAgent;

public class UMCommonHelper {
    private Context context;
    private String appKey, umeng_message_secret;
    private static volatile UMCommonHelper instance = null;

    private UMCommonHelper() {
    }

    public static UMCommonHelper getInstance() {
        synchronized (UMPushHelper.class) {
            if (instance == null) {
                instance = new UMCommonHelper();
            }
            return instance;
        }
    }

    public void preInit(Context context, String appkey, String umeng_message_secret) {
        this.context = context;
        this.appKey = appkey;
        this.umeng_message_secret = umeng_message_secret;

        UMConfigure.preInit(context, appkey, "Umeng");
        PushAgent.setup(context, appkey, umeng_message_secret);
    }

    public void init() {

        UMConfigure.init(
                context,
                appKey,
                "Umeng",
                UMConfigure.DEVICE_TYPE_PHONE,
                umeng_message_secret
        );


    }
}
