/*
 * @Author: shiguo
 * @Date: 2022-05-27 18:27:19
 * @LastEditors: shiguo
 * @LastEditTime: 2022-05-31 16:13:17
 * @FilePath: /@aks-dev/upush/android/src/main/java/com/upush/offline/MfrMessageActivity.java
 */
package com.upush.offline;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.umeng.message.UmengNotifyClickActivity;
import com.upush.R;
import com.upush.UMPushHelper;

import org.android.agoo.common.AgooConstants;

public class MfrMessageActivity extends UmengNotifyClickActivity {

    private static final String TAG = "MfrMessageActivity";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_push);

    }

    @Override
    public void onMessage(Intent intent) {
        super.onMessage(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Log.d(TAG, "bundle: " + bundle);
        }
        String body = intent.getStringExtra(AgooConstants.MESSAGE_BODY);
        Log.d(TAG, "body: " + body);
        if (!TextUtils.isEmpty(body)) {
            runOnUiThread(() -> {
                ((TextView) findViewById(R.id.notification_text)).setText(body);
                // ImageView notification_large_imv = findViewById(R.id.notification_large_icon);
                ImageView notification_small_imv = findViewById(R.id.notification_small_icon);
                int logo = UMPushHelper.getInstance().logo;
                // notification_large_imv.setImageResource(logo);
                notification_small_imv.setImageResource(logo);
            });
        }
    }
}