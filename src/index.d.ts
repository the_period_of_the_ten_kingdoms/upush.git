/*
 * @Author: shiguo
 * @Date: 2022-05-24 11:36:04
 * @LastEditors: shiguo
 * @LastEditTime: 2022-06-06 09:04:02
 * @FilePath: /@aks-dev/upush/src/index.d.ts
 */
import { EmitterSubscription } from 'react-native';
/**
 * sdk冷启动
 * 
 * @description:  init umsdk 返回deviceToken
 * @return {*}
 */
export declare const init: () => Promise<string>;


/**
 * 监听通知事件
 * 
 * 释放：   subscription.remove()
 */
export declare const addEventListener: (listener: (event: any) => void) => EmitterSubscription;


/**
 * 设置角标
 */
export declare const setBadgeCount: (count: number) => void;
