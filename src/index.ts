/*
 * @Author: shiguo
 * @Date: 2022-04-25 18:08:23
 * @LastEditors: shiguo
 * @LastEditTime: 2022-07-25 15:10:39
 * @FilePath: /@aks-dev/upush/src/index.ts
 */


import { NativeModules, Platform, NativeEventEmitter, DeviceEventEmitter, EmitterSubscription } from 'react-native';

const { RNUpushModule, BadgeModule } = NativeModules;


export const init = () => RNUpushModule.init()




export const addEventListener:
    (listener: (event: any) => void) => EmitterSubscription
    = (listener) => {
        if (Platform.OS == 'ios') {
            return new NativeEventEmitter(RNUpushModule).addListener('UMPushNotificationEvent', listener);
        } else {
            /**
             *  new NativeEventEmitter(RNUpushModule) ,也可以监听
             */
            return DeviceEventEmitter.addListener("UMPushNotificationEvent", listener)
        }
    }


export const setBadgeCount = (count: number) => {
    BadgeModule.setCount(count)
}
