<!--
 * @Author: shiguo
 * @Date: 2022-04-25 17:57:29
 * @LastEditors: shiguo
 * @LastEditTime: 2022-09-08 15:10:52
 * @FilePath: /@aks-dev/upush/readme.md
-->

### 安装

`$ npm install @aks-dev/upush --save`

or

`$ yarn add @aks-dev/upush`



### 配置

### android
---

+ <b>MainApplication.onCreate()</b>

```
UMCommonHelper.getInstance().preInit(
        this,
        "6285xxxxxd55d",
        "27b961axxxxxx54cf976");

UMPushHelper.getInstance().preSet(
        this,
        null
);
```
### 离线推送配置
+ <b>buildscript.repositories</b>
```
maven { url 'https://developer.huawei.com/repo/' }
```
+ <b>buildscript.dependencies</b>
```
classpath 'com.huawei.agconnect:agcp:1.6.0.300'
```
+ <b>allprojects.repositories</b>
```
maven { url 'https://developer.huawei.com/repo/' }
maven { url 'https://repo1.maven.org/maven2/' }
```
+ <b>agconnect-services.json</b>
+ <b>AndroidManifest.xml</b>
```
<meta-data
    android:name="com.huawei.hms.client.appid"
    android:value="appid=xxxx" />
<meta-data
    android:name="com.vivo.push.api_key"
    android:value="xxxx" />
<meta-data
    android:name="com.vivo.push.app_id"
    android:value="xxxx" />
```

### ios
---
```
#import "UMPushHelper.h"
#import "UMCommonHelper.h"
```

eg:

+ <b>AppDelegate.application...didFinishLaunchingWithOptions()</b>

```
[[UMCommonHelper share] setAppKey:@"xxx"];
[[UMPushHelper share] registerForRemoteNotificationsWithLaunchOptions:launchOptions];
```

other functions ...

```
//==================push============================================

//iOS10以下使用这两个方法接收通知
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
  [[UMPushHelper share] application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];

}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    [[UMPushHelper share] userNotificationCenter:center willPresentNotification:notification withCompletionHandler:completionHandler];
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    [[UMPushHelper share] userNotificationCenter:center didReceiveNotificationResponse:response withCompletionHandler:completionHandler];
}

/**
 * 获取deviceToken
 */
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  [[UMPushHelper share] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

//==================push============================================

```


### js
---
+ <b>import * as upush from '@aks-dev/upush'</b>

|函数|作用|
|:----|:----|
|init|sdk冷启动,返回deviceToken|
|addListener|监听通知事件|
|setBadgeCount|设置角标|

eg:

```
React.useEffect(() => {
    let subscription = upush.addEventListener((event) => {
        //todo:
    })

    return ()=>{
        subscription.remove()
    }
}, [])
```