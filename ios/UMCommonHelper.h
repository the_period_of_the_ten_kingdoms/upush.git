//
//  UMCommonHelper.h
//  RNUpush
//
//  Created by mac on 2022/5/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UMCommonHelper : NSObject

+(instancetype)share;

-(void)setAppKey:(NSString *) appkey ;
@end

NS_ASSUME_NONNULL_END
