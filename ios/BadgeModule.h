//
//  BadgeModule.h
//  cxhPropertyServer
//
//  Created by mac on 2021/3/31.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
NS_ASSUME_NONNULL_BEGIN

@interface BadgeModule : NSObject<RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
