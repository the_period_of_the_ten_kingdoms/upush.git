
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import <React/RCTEventEmitter.h>

@interface RNUpush : RCTEventEmitter <RCTBridgeModule>

+(void)emitEventPayload:(NSDictionary *)payload;

@end
  
