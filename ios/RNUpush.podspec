
Pod::Spec.new do |s|
  s.name         = "RNUpush"
  s.version      = "1.0.3"
  s.summary      = "RNUpush"
  s.description  = <<-DESC
                  RNUpush
                   DESC
  s.homepage     = "https://gitee.com/the_period_of_the_ten_kingdoms/aks-upush"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "11.0"
  s.source       = { :git => "https://gitee.com/the_period_of_the_ten_kingdoms/aks-upush.git", :tag => "master" }
  s.source_files  = "**/*.{h,m,swift,xib,c}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"
  s.dependency "UMCommon" ,'7.3.7'
  s.dependency "UMPush" ,'4.0.2'
  s.dependency "UMDevice" ,'2.2.1'

end

  