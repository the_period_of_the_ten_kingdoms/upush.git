//
//  UMCommonHelper.m
//  RNUpush
//
//  Created by mac on 2022/5/26.
//

#import "UMCommonHelper.h"
#import <UMCommon/UMConfigure.h>


@interface UMCommonHelper()
@end



@implementation UMCommonHelper


+(instancetype)share{
  static UMCommonHelper * helper = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    if(helper == nil){
        helper = [[self alloc]  init];
    }
  });
  
  return helper;
}
-(id)init{
  self = [super init];
  if(self){

  }
  return  self;
}

-(void)setAppKey:(NSString *) appkey {
    [UMConfigure setLogEnabled:YES];//设置打开日志
    //设置从push后端申请的appkey
    [UMConfigure initWithAppkey:appkey channel:@"App Store"];
}




@end
