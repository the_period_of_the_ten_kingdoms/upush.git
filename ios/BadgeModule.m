//
//  BadgeModule.m
//  cxhPropertyServer
//
//  Created by mac on 2021/3/31.
//

#import "BadgeModule.h"

@implementation BadgeModule
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(setCount:(NSInteger)count)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [UIApplication sharedApplication].applicationIconBadgeNumber = count;
  });
}

@end
